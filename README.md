#
# andlPWM 1.0.0

andlPWM is a library may help you with initializing and using PWM in ATmega microprocessors.
It was created by [andl](https://gitlab.com/.andl).

## Usage policy

andlPWM is free and open-source, you can modify it.

## How do I compile it?

Instruction for compile andlPWM is in release package, in `andlPWM-instruction.pdf` file.

## Functions

For ATmega48A/PA/88A/PA/168A/PA/328/P:
- `void andlPWM_timer0_init(bool OC0A_enabled, bool OC0B_enabled)`
    - Initializes timer0.
    - OC0A_enabled: State of OC0A pin.
    - OC0B_enabled: State of OC0A pin.
- `void andlPWM_timer0_init(bool OC0A_enabled, bool OC0B_enabled)`
    - Initializes timer1.
    - OC1A_enabled: State of OC1A pin.
    - OC1B_enabled: State of OC1A pin.
- `void andlPWM_timer0_init(bool OC0A_enabled, bool OC0B_enabled)`
    - Initializes timer2.
    - OC2A_enabled: State of OC2A pin.
    - OC2B_enabled: State of OC2A pin.

After initialize PWM you can set pin value using OCR0A, OCR0B, OCR1A, OCR1B, OCR2A and OCR2B 8-bit registers.

Example using static library, timer1 and OC1B pin in ATmega328p:

```c
#include <avr/io.h>
#include <util/delay.h>
#include <andlPWM.h>

int main(void)
{
    andlPWM_timer1_init(false, true); // Initialize timer1 and enable OC1B pin.
    
    for (;;)
    {
        OCR1B = 0; // Set OC1B pin to minimal power.
        _delaY_ms(500); // Wait 500 ms.
        OCR1B = 127; // Set OC1B pin to average power.
        _delaY_ms(500); // Wait 500 ms.
        OCR1B = 255; // Set OC1B pin to maximal power.
        _delaY_ms(500); // Wait 500 ms.
    }
}
```
