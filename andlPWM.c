/*
 * ========================================
 * [--------------andlPWM.c---------------]
 * ========================================
 * This source file has definitions of functions which can initialize PWM.
 * It can be used for compile static library or direct include to Your project.
 *
 * Created by andl on 29.03.2019
 */

// ----------------INCLUDES----------------
#include "andlPWM.h"

// ---------------FUNCTIONS----------------
#ifdef __ATmega_48A_PA_88A_PA_168A_PA_328_P

/*
 * Function: andlPWM_timer0_init
 * Initializes timer0.
 * -----------------------------
 * OC0A_enabled: State of OC0A
 *               pin.
 * OC0B_enabled: State of OC0B
 *               pin.
 */
void andlPWM_timer0_init(bool OC0A_enabled, bool OC0B_enabled)
{
	TCCR0A |= (1 << WGM00) | (1 << WGM01); // -----\/
	TCCR0B |= (1 << CS00) | (1 << CS01); // -----> Waveforms generation mode and prescaler select.

	if (OC0A_enabled)
	{
		DDRD |= (1 << PD6); // ------------\/
		TCCR0A |= (1 << COM0A1); // -----> If OC0A_enabled is true, set OC0A pin to output and activate OCR0A register.
	}

	if (OC0B_enabled)
	{
		DDRD |= (1 << PD5); // ------------\/
		TCCR0A |= (1 << COM0B1); // -----> If OC0B_enabled is true, set OC0B pin to output and activate OCR0B register.
	}
}

/*
 * Function: andlPWM_timer1_init
 * Initializes timer1.
 * -----------------------------
 * OC1A_enabled: State of OC1A
 *               pin.
 * OC1B_enabled: State of OC1B
 *               pin.
 */
void andlPWM_timer1_init(bool OC1A_enabled, bool OC1B_enabled)
{
	TCCR1A |= (1 << WGM10); // -----------------------------------\/
	TCCR1B |= (1 << WGM12) | (1 << CS10) | (1 << CS11); // -----> Waveforms generation mode and prescaler select.

	if (OC1A_enabled)
	{
		DDRB |= (1 << PB1); // ------------\/
		TCCR1A |= (1 << COM1A1); // -----> If OC1A_enabled is true, set OC1A pin to output and activate OCR1A register.
	}

	if (OC1B_enabled)
	{
		DDRB |= (1 << PB2); // ------------\/
		TCCR1A |= (1 << COM1B1); // -----> If OC1B_enabled is true, set OC1B pin to output and activate OCR1B register.
	}
}

/*
 * Function: andlPWM_timer2_init
 * Initializes timer2.
 * -----------------------------
 * OC2A_enabled: State of OC2A
 *               pin.
 * OC2B_enabled: State of OC2B
 *               pin.
 */
void andlPWM_timer2_init(bool OC2A_enabled, bool OC2B_enabled)
{
	TCCR2A |= (1 << WGM20) | (1 << WGM21); // -----\/
	TCCR2B |= (1 << CS22); // -------------------> Waveforms generation mode and prescaler select.

	if (OC2A_enabled)
	{
		DDRB |= (1 << PB3); // ------------\/
		TCCR2A |= (1 << COM2A1); // -----> If OC2A_enabled is true, set OC2A pin to output and activate OCR2A register.
	}

	if (OC2B_enabled)
	{
		DDRD |= (1 << PD3); // ------------\/
		TCCR2A |= (1 << COM2B1); // -----> If OC2B_enabled is true, set OC2B pin to output and activate OCR2B register.
	}
}

#endif
