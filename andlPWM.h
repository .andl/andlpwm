/*
 * ========================================
 * [--------------andlPWM.h---------------]
 * ========================================
 * This header file has declarations of functions which can initialize PWM.
 * It can be used for compile static library or direct include to Your project.
 *
 * Created by andl on 29.03.2019
 */

// -------------INCLUDE GUARD--------------
#ifndef ANDLPWM_H
#define ANDLPWM_H

// ----------------INCLUDES----------------
#include <avr/io.h>
#include <stdbool.h>

// -------------CONFIGURATION--------------
#define __ATmega_48A_PA_88A_PA_168A_PA_328_P (defined __AVR_ATmega48A__) || (defined __AVR_ATmega48PA__) || (defined __AVR_ATmega88A__) || (defined __AVR_ATmega88PA__) || (defined __AVR_ATmega168A__) || (defined __AVR_ATmega168PA__) || (defined __AVR_ATmega328__) || (defined __AVR_ATmega328P__)

// ---------------FUNCTIONS----------------
#ifdef __ATmega_48A_PA_88A_PA_168A_PA_328_P

/*
 * Function: andlPWM_timer0_init
 * Initializes timer0.
 * -----------------------------
 * OC0A_enabled: State of OC0A
 *               pin.
 * OC0B_enabled: State of OC0B
 *               pin.
 */
void andlPWM_timer0_init(bool OC0A_enabled, bool OC0B_enabled);

/*
 * Function: andlPWM_timer1_init
 * Initializes timer1.
 * -----------------------------
 * OC1A_enabled: State of OC1A
 *               pin.
 * OC1B_enabled: State of OC1B
 *               pin.
 */
void andlPWM_timer1_init(bool OC1A_enabled, bool OC1B_enabled);

/*
 * Function: andlPWM_timer2_init
 * Initializes timer2.
 * -----------------------------
 * OC2A_enabled: State of OC2A
 *               pin.
 * OC2B_enabled: State of OC2B
 *               pin.
 */
void andlPWM_timer2_init(bool OC2A_enabled, bool OC2B_enabled);

#endif

#endif // ANDLPWM_H
